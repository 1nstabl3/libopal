/**
 * @file barometer.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief Generic barometer driver implementation 
 * @version 0.1
 * @date 2020-12-29
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <stdint.h>

/* HAL Includes
 * ----------------------
 */
#include "sensor_base.hpp"

/* App Includes
 * ----------------------
 */


/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */
// LOG_MODULE_REGISTER(IMU, LOG_LEVEL_DBG);


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */
namespace opal { namespace Sensor
{
	template< class DRIVER >
	class Barometer : public SensorBase
	{
	public:
		Barometer(){}


	};
} }