/**
 * @file models.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief Implementation of different models for flight physics or board systems
 * @version 0.1
 * @date 2020-07-28
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <stdint.h>
#include <array>
#include <vector>
#ifdef __EXCEPTIONS
#include <exception>
#else
#include <cassert>
#endif 
#include <chrono>
#include <sstream>
#include <iostream>
#include <iomanip>

/* HAL Includes
 * ----------------------
 */
#include <logging/log.h>

/* App Includes
 * ----------------------
 */
#include <Eigen/Dense>

/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */
// LOG_MODULE_REGISTER(MODELS, LOG_LEVEL_DBG);


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */
namespace Models
{
	template<uint_fast8_t STATE_DIM, uint_fast8_t INPUT_DIM>
	class BaseModel
	{
	public:
		BaseModel(){};
	
		virtual Eigen::Vector<float, STATE_DIM> get_model_derivative(	const Eigen::Vector<float, STATE_DIM>& curr_state,
																		const Eigen::Vector<float, INPUT_DIM>& inputs) = 0;
	};
	
};