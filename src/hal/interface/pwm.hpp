/**
 * @file bitbang.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief Bitbang bus driver implementation for custom bus protocols
 * @version 0.1
 * @date 2021-03-07
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <cstdint>

/* HAL Includes
 * ----------------------
 */

/* App Includes
 * ----------------------
 */
// #include "hal/primitives/address.hpp"

/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */
namespace opal { namespace Hardware { namespace Interface { namespace PWM
{
	template<class DRIVER, class TIMER, class PIN >
	class Instance
	{
		public: 
			Instance(){}
	
			void init()
			{

			};

			// static void set_pins(void)
			// {
			// 	DRIVER::template I2CDriver<INST, SDA, SCL>::set_pins();
			// }
			template<class PROTOCOL, typename T>
			static void write(const T& val)
			{
				DRIVER::template PWM<PIN>::write(PROTOCOL::encode(val));
			}

			template<typename T>
			static T read()
			{
				return T();
			}

			/**
			 * @brief Writes n bytes to the requested remote address
			 * 
			 * Sets the remote memory pointer to the requested address and 
			 * initiates a subsequent write of byte_cnt bytes or until
			 * the remote stops the transmission. The actual number of 
			 * written bytes is returned. 
			 * 
			 * @tparam ADDR 
			 * @tparam REG_ADDR 
			 * @param dev_addr 
			 * @param reg_addr 
			 * @param source 
			 * @param byte_cnt 
			 * @return int 
			 */
			template<class ADDR, class REG_ADDR>
			static int continous_write(const ADDR& dev_addr, const REG_ADDR& reg_addr, uint8_t* source, size_t byte_cnt)
			{
				
			}
	};
	
} } }