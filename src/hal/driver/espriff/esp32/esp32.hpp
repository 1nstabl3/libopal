/**
 * @file esp32.hpp
 * @author Patrick Günzel (patrickguenzel@outlook.com)
 * @brief Driver implementation for the Espriff ESP32 SoC to be used by the opal HAL
 * @version 0.1
 * @date 2021-01-10
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#pragma once

#include <cstdint>
#include <type_traits>

#include "hal/primitives/register.hpp"
#include "hal/primitives/address.hpp"
#include "hal/primitives/field.hpp"
#include "hal/interface/gpio.hpp"

namespace opal { namespace Hardware { namespace Driver
{
	class ESP32 
	{
			ESP32(){};

			template<class ADDR_BASE, uint32_t REG_ADDR, uint32_t DEFAULT_VAL>
			using REG = Register<Address<ADDR_BASE, REG_ADDR>, uint32_t, DEFAULT_VAL>;

			template<class REGISTER, uint_fast8_t OFFSET, uint_fast32_t BITLENGTH>
			using FLD = opal::Hardware::BitField<REGISTER, OFFSET, BITLENGTH>; 

			template<class REGISTER, uint_fast8_t OFFSET>
			using FLD1 = opal::Hardware::SingleBitField<REGISTER, OFFSET>; 

		public:
			template<class PORT, class PIN>
			class GPIOPeripheral
			{
				GPIOPeripheral(){}

				public:	// Register Definition
					typedef AddressSpace<0x50000000> PORT0_BASE;
					typedef AddressSpace<0x50000300> PORT1_BASE;

				/* select an AddressSpace based on the PORT value */
				typedef typename std::conditional<PORT::value, PORT0_BASE, PORT1_BASE>::type PORT_INSTANCE;

					/*  OUT         0x504   Write GPIO port
						OUTSET      0x508   Set individual bits in GPIO port
						UTCLR       0x50C   Clear individual bits in GPIO port
						IN          0x510   Read GPIO port
						DIR         0x514   Direction of GPIO pins
						DIRSET      0x518   DIR set register
						DIRCLR      0x51C   DIR clear register
						LATCH       0x520   Latch register indicating what GPIO pins that have met the criteria set in the PIN_CNF[n].SENSEregisters
						DETECTMODE  0x524   Select between default DETECT signal behaviour and LDETECT mode
						PIN_CNF[0]  0x700   Configuration of GPIO pins
						PIN_CNF[1]  0x704   Configuration of GPIO pins
						...
						PIN_CNF[31] 0x77C   Configuration of GPIO pins
					 */
					typedef REG<PORT_INSTANCE, 0x504, 0x00>                             OUT;
					typedef FLD1<OUT, PIN::value> 			OUT_DRIVE_STATE;      		/**< Set to 1 if GPIO configured as output AND set to HIGH */
					typedef REG<PORT_INSTANCE, 0x508, 0x00>                             OUTSET;
					typedef FLD1<OUTSET, PIN::value> 		OUT_SET;					/**<  */
					typedef REG<PORT_INSTANCE, 0x50C, 0x00>                             UTCLR;
					typedef FLD1<UTCLR, PIN::value> 		OUT_CLEAR;					/**<  */
					typedef REG<PORT_INSTANCE, 0x510, 0x00>                             IN;
					typedef FLD1<IN, PIN::value> 			IN_STATE;					/**<  */
					typedef REG<PORT_INSTANCE, 0x514, 0x00>                             DIR;
					typedef FLD1<DIR, PIN::value> 			PIN_DIR;					/**<  */
					typedef REG<PORT_INSTANCE, 0x518, 0x00>                             DIRSET;
					typedef FLD1<DIRSET, PIN::value> 		PIN_DIR_SET;				/**<  */
					typedef REG<PORT_INSTANCE, 0x51C, 0x00>                             DIRCLR;
					typedef FLD1<DIRCLR, PIN::value> 		PIN_DIR_CLEAR;				/**<  */
					typedef REG<PORT_INSTANCE, 0x520, 0x00>                             LATCH;
					typedef FLD1<LATCH, PIN::value> 		PIN_LATCH_STATE;			/**<  */
					typedef REG<PORT_INSTANCE, 0x524, 0x00>                             DETECTMODE;
					typedef FLD1<DETECTMODE, PIN::value> 	PIN_DETECT_MODE;			/**<  */
					typedef REG<PORT_INSTANCE, 0x700 + (PIN::value * 0x4), 0x00000002>  PIN_CFG;
					typedef FLD1<PIN_CFG, 0> 				CFG_PIN_DIR;				/**<  */
					typedef FLD1<PIN_CFG, 1> 				CFG_PIN_BUFFERED;			/**<  */
					typedef FLD<PIN_CFG, 2, 2> 				CFG_PIN_PULL;				/**<  */
					typedef FLD<PIN_CFG, 8, 3> 				CFG_PIN_DRIVE;				/**<  */
					typedef FLD<PIN_CFG, 16, 2> 			CFG_PIN_SENSE;				/**<  */

				public:
					static void set(const bool val)
					{
						OUT_SET::set(val);
					}

					static const bool get()
					{
						return IN_STATE::get();
					}

					static void configure(	const Interface::GPIO::DIR	 			io_dir, 
											const Interface::GPIO::PULL 			io_pull, 
											const Interface::GPIO::IRQ 				io_irq, 
											const Interface::GPIO::DRIVE_STRENGTH 	io_pwr)
					{
						uint32_t cfg_reg_val = 0;
						cfg_reg_val |= CFG_PIN_DIR::prepare(_map_from_direction(io_dir));
						cfg_reg_val |= CFG_PIN_PULL::prepare(_map_from_pull(io_pull));
						cfg_reg_val |= CFG_PIN_DRIVE::prepare(_map_from_drive_strength(io_pwr));
						cfg_reg_val |= CFG_PIN_SENSE::prepare(_map_from_irq(io_irq));
						PIN_CFG::set(cfg_reg_val);
					}

					static void get_configuration()
					{
						uint32_t 				cfg_reg_val = PIN_CFG::get();
						Interface::GPIO::DIR 				io_dir 		= _map_to_direction(CFG_PIN_DIR::extract(cfg_reg_val));
						Interface::GPIO::PULL 				io_pull 	= _map_to_pull(CFG_PIN_PULL::extract(cfg_reg_val));
						Interface::GPIO::IRQ 				io_irq 		= _map_to_irq(CFG_PIN_SENSE::extract(cfg_reg_val));
						Interface::GPIO::DRIVE_STRENGTH 	io_pwr 		= _map_to_drive_strength(CFG_PIN_DRIVE::extract(cfg_reg_val));
					}

				private:
					static uint32_t _map_from_direction(const Interface::GPIO::DIR io_dir) { return static_cast<uint32_t>(io_dir); }
					static Interface::GPIO::DIR _map_to_direction(const uint32_t val) { return static_cast<Interface::GPIO::DIR>(val); }

					static uint32_t _map_from_pull(const Interface::GPIO::PULL io_pull) 
					{
						switch(io_pull)
						{
							case Interface::GPIO::PULL::UP:		return 3;
							case Interface::GPIO::PULL::DOWN: 	return 1;
							default:
							case Interface::GPIO::PULL::NONE: 	return 0;
						}
					}
					static Interface::GPIO::PULL _map_to_pull(const uint32_t val)
					{
						switch(val)
						{
							case 3: return Interface::GPIO::PULL::UP;
							case 1: return Interface::GPIO::PULL::DOWN;
							default:
							case 0:	return Interface::GPIO::PULL::NONE;
						}
					}

					static uint32_t _map_from_drive_strength(const Interface::GPIO::DRIVE_STRENGTH io_pwr)
					{
						return static_cast<uint32_t>(io_pwr);
					}

					static Interface::GPIO::DRIVE_STRENGTH _map_to_drive_strength(const uint32_t val)
					{
						return static_cast<Interface::GPIO::DRIVE_STRENGTH>(val);
					}

					static uint32_t _map_from_irq(const Interface::GPIO::IRQ io_irq)
					{
						switch(io_irq)
						{
							case Interface::GPIO::IRQ::HIGH_LEVEL:	return 2;
							case Interface::GPIO::IRQ::LOW_LEVEL: 	return 3;
							default:
							case Interface::GPIO::IRQ::NONE: 		return 0;
						}
					}
					static Interface::GPIO::IRQ _map_to_irq(const uint32_t val)
					{
						switch(val)
						{
							case 2: return Interface::GPIO::IRQ::HIGH_LEVEL;
							case 3: return Interface::GPIO::IRQ::LOW_LEVEL;
							default:
							case 0: return Interface::GPIO::IRQ::NONE;
						}
					}
			};

	};

} } }