/**
 * @file mockup_random_walk.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief Implements a mockup sensor drier, that supplies a random walk sensor reading
 * @version 0.1
 * @date 2020-11-15
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <stdint.h>
#include <functional>

/* HAL Includes
 * ----------------------
 */
#include <zephyr.h>
#include <logging/log.h>

/* App Includes
 * ----------------------
 */


/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */
// LOG_MODULE_REGISTER(IMU, LOG_LEVEL_DBG);


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */
namespace opal { namespace Hardware { namespace Driver
{
	template< typename T, T INIT_VAL, T WALKING_DIST, uint_fast32_t CH_CNT = 1 >
	class MockupRandomWalk
	{
	public:
		MockupRandomWalk(){}
	};

} } }