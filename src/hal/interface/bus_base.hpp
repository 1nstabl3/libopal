/**
 * @file bus_base.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief Base class implementation for a peripheral bus
 * @version 0.1
 * @date 2020-11-15
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <stdint.h>
#include <functional>

/* HAL Includes
 * ----------------------
 */

/* App Includes
 * ----------------------
 */


/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */
// LOG_MODULE_REGISTER(IMU, LOG_LEVEL_DBG);


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */
namespace opal { namespace Hardware { namespace Interface 
{
	class BusBase
	{
			BusBase(){}
		public:

		template<typename T, class BUS_SELECTOR, class REG_ADDR>
		static T get(const BUS_SELECTOR& selector, const REG_ADDR& addr) { return T(); }
	};	
	
} } }