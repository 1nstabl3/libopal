/**
 * @file gnss.hpp
 * @author Patrick Günzel (patrickguenzel@outlook.com)
 * @brief Generic Global Navigation Satellite System (GNSS) driver
 * @version 0.1
 * @date 2020-12-29
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <stdint.h>
#include <functional>

/* HAL Includes
 * ----------------------
 */
#include "sensor_base.hpp"

/* App Includes
 * ----------------------
 */


/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */
// LOG_MODULE_REGISTER(IMU, LOG_LEVEL_DBG);


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */
namespace opal { namespace Sensor
{
	template< class DRIVER >
	class GNSS : public SensorBase
	{
	public:
		GNSS(){}


	};
} }