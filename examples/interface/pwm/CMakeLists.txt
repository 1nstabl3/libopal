# SPDX-License-Identifier: Apache-2.0
cmake_minimum_required(VERSION 3.13.1)


set(BOARD qemu_cortex_m3)

set(SOFT_VER_MAJOR 0)
set(SOFT_VER_MINOR 1)
set(SOFT_VER_PATCH 0)

set(LIBOPAL_INCLUDE_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/../../../src/)

# Set a default build type if none was specified
set(default_build_type "Release")
# if(EXISTS "${CMAKE_SOURCE_DIR}/.git")
	# set(default_build_type "Debug")
# endif()

if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
	message(STATUS "Setting build type to '${default_build_type}' as none was specified.")
	set(CMAKE_BUILD_TYPE "${default_build_type}" CACHE
			STRING "Choose the type of build." FORCE)
	# Set the possible values of build type for cmake-gui
	set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS
		"Debug" "Release")
endif()






find_package(Zephyr REQUIRED HINTS $ENV{ZEPHYR_BASE})

project(opal-pwm)


set(CMAKE_CXX_STANDARD 20)

target_include_directories(app PRIVATE src/
			                ${LIBOPAL_INCLUDE_DIRECTORY})


target_sources(app PRIVATE src/main.cpp)
