/**
 * @file i2c.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief I2C bus driver implementation
 * @version 0.1
 * @date 2020-11-15
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <stdint.h>
#include <functional>

/* HAL Includes
 * ----------------------
 */

/* App Includes
 * ----------------------
 */
#include "hal/primitives/address.hpp"

/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */
namespace opal { namespace Hardware { namespace Interface { namespace I2C
{
	template< uint_fast32_t INIT_BAUD>
	class BaudRate
	{
		BaudRate() {}
		public:
			static constexpr auto value = INIT_BAUD;
	};

	enum class BAUD_RATE : uint8_t
	{
		B100KBPS = 0,
		B250KBPS = 1,
		B400KBPS = 2
	};

	template<class DRIVER, class INST, class SDA, class SCL >
	class Instance
	{
		public: 
			Instance(){}
	
			void init(const BaudRate& baud_rate)
			{

			};

			static void set_pins(void)
			{
				DRIVER::template I2CDriver<INST, SDA, SCL>::set_pins();
			}

			/**
			 * @brief Reads a T-sized value from the requested register address
			 * 
			 * Sets the remote memory pointer to the requested address and initiates
			 * a subsequent read with siezof(T) bytes and returns the result. It is the users
			 * responsebility to ensure that the remote source can read
			 * an apropriate value.  
			 * 
			 * @tparam T 
			 * @tparam ADDR 
			 * @tparam REG_ADDR 
			 * @param dev_addr 
			 * @param reg_addr 
			 * @return T 
			 */
			template<typename T, class ADDR, class REG_ADDR>
			static T read(const ADDR& dev_addr, const REG_ADDR& reg_addr)
			{
				
			}
			
			/**
			 * @brief Writes a T-sized value to the requested register address
			 * 
			 * Sets the remote memory pointer to the requested address and initiates
			 * a subsequent write with siezof(T) bytes. It is the users
			 * responsebility to ensure that the remote source can store
			 * an apropriate value.
			 * 
			 * @tparam T 
			 * @tparam ADDR 
			 * @tparam REG_ADDR 
			 * @param dev_addr 
			 * @param reg_addr 
			 * @param value 
			 */
			template<typename T, class ADDR, class REG_ADDR>
			static void write(const ADDR& dev_addr, const REG_ADDR& reg_addr, const T& value)
			{
				
			}

			/**
			 * @brief Reads n bytes from the requested remote address
			 * 
			 * Sets the remote memory pointer to the requested address and 
			 * initiates a subsequent read with byte_cnt bytes or until
			 * the remote stops the transmission. The actual number of 
			 * read bytes is returned. 
			 * 
			 * @tparam ADDR 
			 * @tparam REG_ADDR 
			 * @param dev_addr 
			 * @param reg_addr 
			 * @param target 
			 * @param byte_cnt 
			 * @return int 
			 */
			template<class ADDR, class REG_ADDR>
			static int continous_read(const ADDR& dev_addr, const REG_ADDR& reg_addr, uint8_t* target, size_t byte_cnt)
			{
				
			}

			/**
			 * @brief Writes n bytes to the requested remote address
			 * 
			 * Sets the remote memory pointer to the requested address and 
			 * initiates a subsequent write of byte_cnt bytes or until
			 * the remote stops the transmission. The actual number of 
			 * written bytes is returned. 
			 * 
			 * @tparam ADDR 
			 * @tparam REG_ADDR 
			 * @param dev_addr 
			 * @param reg_addr 
			 * @param source 
			 * @param byte_cnt 
			 * @return int 
			 */
			template<class ADDR, class REG_ADDR>
			static int continous_write(const ADDR& dev_addr, const REG_ADDR& reg_addr, uint8_t* source, size_t byte_cnt)
			{
				
			}
	};
	
} } }