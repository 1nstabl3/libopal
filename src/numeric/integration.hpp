/**
 * @file numerics.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief Implementation of various numeric methods for models and time integration
 * @version 0.1
 * @date 2020-05-31
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <stdint.h>
#include <array>
#include <vector>
#ifdef __EXCEPTIONS
#include <exception>
#else
#include <cassert>
#endif 
#include <chrono>
#include <sstream>
#include <iostream>
#include <iomanip>

/* HAL Includes
 * ----------------------
 */
#include <logging/log.h>

/* App Includes
 * ----------------------
 */
#include <Eigen/Dense>
#include "modules/models/models.hpp"

/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */
// LOG_MODULE_REGISTER(NUMERIC, LOG_LEVEL_DBG);


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */

namespace Numeric
{
	namespace Integration
	{
		enum class RUNGEKUTTA
		{
			ORDER2 = 2,
			ORDER4 = 4
		};
	
		template<uint_fast8_t SD, uint_fast8_t ID, class MODEL = Models::BaseModel<SD, ID>>
		class ExplicitRungeKutta : private MODEL
		{
		private:
			static constexpr uint_fast8_t 	INPUT_HISTORY_CNT = 3;
			RUNGEKUTTA 												_order;

			Eigen::Vector<float, SD> 								_states;
			std::array<Eigen::Vector<float, ID>, INPUT_HISTORY_CNT> _inputs;
			uint_fast8_t 											_current_slot;
			bool 													_half_step;
			std::chrono::system_clock::time_point 					_last_iteration;
			std::chrono::microseconds								_t;	// full time step [s]
			std::chrono::microseconds								_t2; 	// half time step [s]

			uint_fast8_t get_current_index() const { return _current_slot; }
			uint_fast8_t get_half_index() const 
			{ 
				int_fast8_t ind = _current_slot - 1;
				if(ind >= 0)
					return ind;
				else
					return INPUT_HISTORY_CNT + ind; 
			}
			uint_fast8_t get_last_index() const
			{ 
				int_fast8_t ind = _current_slot - 2;
				if(ind >= 0)
					return ind;
				else
					return INPUT_HISTORY_CNT + ind; 
			}
		public:
			ExplicitRungeKutta(const RUNGEKUTTA order = RUNGEKUTTA::ORDER2) : _order(order), _current_slot(0), _half_step(true) {};

			void init_model(const Eigen::Vector<float, SD>& z0)
			{
				_states = z0;
			};
			const Eigen::Vector<float, SD>& integrate(const Eigen::Vector<float, ID>& input)
			{

				_current_slot++;
				if(_current_slot >= INPUT_HISTORY_CNT)
					_current_slot = 0;	

				_inputs[_current_slot] = input;

				if(!_half_step)
				{ 
					_t = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - _last_iteration);
					_last_iteration = std::chrono::system_clock::now();

					switch(_order)
					{
						case RUNGEKUTTA::ORDER2:
							_states = _states + (std::chrono::duration_cast<std::chrono::seconds>(_t).count() * k2());
							break;
						case RUNGEKUTTA::ORDER4:
							_states = _states + ((std::chrono::duration_cast<std::chrono::seconds>(_t).count() / 6.0f) * (k1() + (2*k2()) + (2*k3()) + k4() ));
							break;
						default: 
						#ifdef __EXCEPTIONS
							throw std::runtime_error("Not implemented");
						#else
							assert(0 == 1);
						#endif
					}	
				}
				
				_half_step = !_half_step;
				return _states;
			};

		private:
			Eigen::Vector<float, SD> k1() /*const*/ { return MODEL::get_model_derivative(_states, _inputs[get_last_index()]); }
			Eigen::Vector<float, SD> k2() /*const*/ { return MODEL::get_model_derivative(_states + ((std::chrono::duration_cast<std::chrono::seconds>(_t).count() / 2.0f) * k1()), _inputs[get_half_index()]); }
			Eigen::Vector<float, SD> k3() /*const*/ { return MODEL::get_model_derivative(_states + ((std::chrono::duration_cast<std::chrono::seconds>(_t).count()/2.0f) * k2()), _inputs[get_half_index()]); }
			Eigen::Vector<float, SD> k4() /*const*/ { return MODEL::get_model_derivative(_states + (std::chrono::duration_cast<std::chrono::seconds>(_t).count() * k3()), _inputs[get_current_index()]); }
		};		
	};
}