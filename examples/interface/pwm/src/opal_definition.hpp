
#pragma once

#include "hal/hal.hpp"

typedef opal::Hardware::Driver::NRF52840 SOC_t;

typedef opal::Hardware::Interface::GPIO::Port<0> PORT0_t;
typedef opal::Hardware::Interface::GPIO::Pin<26> PIN26_t;
typedef opal::Hardware::Interface::GPIO::Pin<27> PIN26_t;
typedef opal::Hardware::Interface::GPIO::Pin<28> PIN26_t;

typedef opal::Hardware::Interface::GPIO::Instance<SOC_t, PORT0_t, PIN26_t> GPIO0_26;
typedef opal::Hardware::Interface::GPIO::Instance<SOC_t, PORT0_t, PIN27_t> GPIO0_27;
typedef opal::Hardware::Interface::GPIO::Instance<SOC_t, PORT0_t, PIN28_t> GPIO0_28;

typedef opal::Hardware::Interface::PWM::Instance<SOC_t, GPIO0_26, 400000> PWM0;
typedef opal::Hardware::Interface::PWM::SPISoftwareInstance<SOC_t, GPIO0_27, 400000> PWM1;
typedef opal::Hardware::Interface::PWM::TimerSoftwareInstance<SOC_t, GPIO0_28, 400000> PWM2;

typedef opal::Hardware::Protocol::WS2812::Encoder WS2812;

typedef opal::Hardware::Interface::ShiftRegister::Instance<PWM1, WS2812, 30> LEDLine0;
typedef opal::Video::Display::Instance<LEDLine0> Ring0;
typedef opal::Video::FrameBuffer::Instance<Ring0> Scene0;