/**
 * @file imu.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief Sensor implementation for the Intertial Measurement Unit (IMU)
 * @version 0.1
 * @date 2020-07-28
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <cstdint>

/* HAL Includes
 * ----------------------
 */
#include "sensor_base.hpp"

/* App Includes
 * ----------------------
 */


/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */
// LOG_MODULE_REGISTER(IMU, LOG_LEVEL_DBG);


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */
namespace opal { namespace Sensor
{
	template< class DRIVER >
	class IMU : public SensorBase
	{
	public:
		IMU(){}

	private:
		
	};

} }