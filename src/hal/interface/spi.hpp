/**
 * @file spi.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief SPI implementation
 * @version 0.1
 * @date 2020-11-15
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <stdint.h>
#include <functional>

/* HAL Includes
 * ----------------------
 */

/* App Includes
 * ----------------------
 */
#include "bus_base.hpp"


/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */
// LOG_MODULE_REGISTER(IMU, LOG_LEVEL_DBG);


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */
namespace opal { namespace Hardware { namespace Interface 
{
	class SPI : public BusBase
	{
		public: 
			SPI(){}
	};
	
} } }