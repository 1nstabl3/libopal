/**
 * @file numerics.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief Implementation of various numeric methods for models and time integration
 * @version 0.1
 * @date 2020-05-31
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <stdint.h>
#include <array>
#include <vector>
#ifdef __EXCEPTIONS
#include <exception>
#else
#include <cassert>
#endif 
#include <chrono>
#include <sstream>
#include <iostream>
#include <iomanip>

/* HAL Includes
 * ----------------------
 */
#include <logging/log.h>

/* App Includes
 * ----------------------
 */
#include <Eigen/Dense>
#include "modules/models/models.hpp"

/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */
// LOG_MODULE_REGISTER(NUMERIC, LOG_LEVEL_DBG);


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */

namespace Numeric
{
	namespace Interpolation
	{
		class CatmullRomSplineXd
		{
			typedef Eigen::Vector<float, 2> point_t;
		private:
			float _tension;
			std::vector<std::array<float, 4> > 	_tangents;
			std::vector<point_t> 				_ctrl_points;
			Eigen::Matrix<float, 4,4> M;

		public:
			CatmullRomSplineXd(const std::vector<point_t>& ctrl_points, float tension)
			{
				M << 	0,    1,    0,   0,
						-0.5, 0,    0.5, 0,
						1,    -2.5, 2,   -0.5,
						-0.5, 1.5, -1.5, 0.5;
				set(ctrl_points, tension);
			}

			void set(const std::vector<point_t>& ctrl_points, float tension)
			{
				_tension = tension;
				if(_tension < 0)
					_tension *= (-1);

				if(ctrl_points.size() < 2)
				{
					#ifdef __EXCEPTIONS
						throw std::runtime_error("At least two control points have to be given");
					#else
						assert(0 == 1);
					#endif
				}
				_ctrl_points.resize(ctrl_points.size() + 2);
				_ctrl_points[0] = (2 * ctrl_points[0]) - ctrl_points[1];
				_ctrl_points[_ctrl_points.size() - 1] = (2 * ctrl_points[ctrl_points.size() - 1]) - ctrl_points[ctrl_points.size() - 2];
				std::copy(ctrl_points.begin(), ctrl_points.end(), _ctrl_points.begin() + 1);
				for(auto p : _ctrl_points)
					std::cout << "P: (" << p << ")" << std::endl;

				_tangents.resize(_ctrl_points.size() - 3);
				for(auto&& t : _tangents)
					t[0] = 0;

				std::cout << "Tangents:" << std::endl;
				for(uint16_t it = 0; it < _ctrl_points.size() - 3; ++it)
				{
					std::cout << std::setw(5) << std::setprecision(3) << _tangents[it][0] << " ";
					for(uint16_t i = 1; i < _tangents[0].size(); ++i)
					{
						_tangents[it][i] = tj(_tangents[it][i-1], _ctrl_points[it+i-1], _ctrl_points[it+i]);
						std::cout << std::setw(5) << std::setprecision(3) << _tangents[it][i] << " ";
					}
					std::cout << std::endl;
				}	
			};

			float calculate(float x)
			{
				if(x <= _ctrl_points[1][0])
					return _ctrl_points[1][1];
				else if(x >= _ctrl_points[_ctrl_points.size() - 2][0])
					return _ctrl_points[_ctrl_points.size() - 2][1];
				
				/* Find applying section */
				uint16_t section_index = 1;
				// std::cout << "with " << (int)_ctrl_points.size() << " control points" << std::endl;
				for(;section_index < _ctrl_points.size()-1; ++section_index)
				{
					// std::cout << "i=" << std::to_string(section_index) << " v=" << _ctrl_points[section_index][0] << std::endl;
					if(x <= _ctrl_points[section_index][0])
						break;
				}
				section_index--;
				// std::cout << "Value x=" << x << "applies to section i=" << std::to_string(section_index) << std::endl;
				std::cout << std::to_string(section_index) << "=";

				/* Calc intermediates */
				float fractional = x - std::floor(x);
				Eigen::Vector4f u = { 1, fractional, std::pow(fractional, 2), std::pow(fractional, 3)};
				
				auto i_t = section_index - 1;
				auto i_p = section_index;
				auto A1 = (_tangents[i_t][1]-x)/(_tangents[i_t][1]-_tangents[i_t][0])*_ctrl_points[i_p + 0][1] + (x-_tangents[i_t][0])/(_tangents[i_t][1]-_tangents[i_t][0])*_ctrl_points[i_p + 1][1];
				auto A2 = (_tangents[i_t][2]-x)/(_tangents[i_t][2]-_tangents[i_t][1])*_ctrl_points[i_p + 1][1] + (x-_tangents[i_t][1])/(_tangents[i_t][2]-_tangents[i_t][1])*_ctrl_points[i_p + 2][1];
				auto A3 = (_tangents[i_t][3]-x)/(_tangents[i_t][3]-_tangents[i_t][2])*_ctrl_points[i_p + 2][1] + (x-_tangents[i_t][2])/(_tangents[i_t][3]-_tangents[i_t][2])*_ctrl_points[i_p + 3][1];

				auto B1 = (_tangents[i_t][2]-x)/(_tangents[i_t][2]-_tangents[i_t][0])*A1 + (x-_tangents[i_t][0])/(_tangents[i_t][2]-_tangents[i_t][0])*A2;
    			auto B2 = (_tangents[i_t][3]-x)/(_tangents[i_t][3]-_tangents[i_t][1])*A2 + (x-_tangents[i_t][1])/(_tangents[i_t][3]-_tangents[i_t][1])*A3;

				// /* Calc result */
    			auto C = (_tangents[section_index-1][2]-x)/(_tangents[section_index-1][2]-_tangents[section_index-1][1])*B1 + (x-_tangents[section_index-1][1])/(_tangents[section_index-1][2]-_tangents[section_index-1][1])*B2;
				return C;
			};
		
		private:
			inline float tj(const float& ti, const point_t& Pi,const point_t& Pj )
			{
				return std::pow((Pi - Pj).squaredNorm(), _tension/2.0f) + ti;
			}
		};		
	}
}