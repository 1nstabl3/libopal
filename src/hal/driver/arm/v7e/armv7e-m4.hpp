/**
 * @file armv7e-m4.hpp
 * @author Patrick Günzel (patrickguenzel@outlook.com)
 * @brief Driver implementation for the ARMv7e-M4 core
 * @version 0.1
 * @date 2021-01-29
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#pragma once

#include <cstdint>
#include <type_traits>

#include "hal/primitives/register.hpp"
#include "hal/primitives/address.hpp"
#include "hal/primitives/field.hpp"

namespace opal { namespace Hardware { namespace Driver
{
	class ARMv7eM4 
	{
			ARMv7eM4(){};

			template<class ADDR_BASE, uint32_t REG_ADDR, uint32_t DEFAULT_VAL>
			using REG = Register<Address<ADDR_BASE, REG_ADDR>, uint32_t, DEFAULT_VAL>;

			template<class REGISTER, uint_fast8_t OFFSET, uint_fast32_t BITLENGTH>
			using FLD = opal::Hardware::BitField<REGISTER, OFFSET, BITLENGTH>; 

			template<class REGISTER, uint_fast8_t OFFSET>
			using FLD1 = opal::Hardware::SingleBitField<REGISTER, OFFSET>; 

		public:
			
	};

} } }