#pragma once

#include <cstdint>
#if __cplusplus >= 202002L
#include <concepts>
#endif // c++20

#include "hal/primitives/address.hpp"
namespace opal { namespace Hardware 
{ 
	template<IsAnAddress REG_ADDR, typename T, T DEFAULT_VAL>
	class Register
	{
		Register(){}

	public:
		typedef T register_value_type_t;

		static T get(void)
		{
			// return static_cast<T>(*(reinterpret_cast<T*>(reinterpret_cast<std::uintptr_t>(REG_ADDR::value))));
			return static_cast<T>(*(reinterpret_cast<T*>(REG_ADDR::value)));
		}

		static void set(const T val)
		{
			*(reinterpret_cast<T*>(REG_ADDR::value)) = static_cast<T>(val);
		}

		static constexpr auto address = REG_ADDR::value;

		static constexpr T default_value(void) { return DEFAULT_VAL; } 

		static bool is_default(void) { return get() == default_value(); }
	};


	/**
	 * @brief Opaque overlay over a remote register (e.g. inside a chip connected via I2C or SPI)
	 * 
	 * @ingroup Hardware
	 * 
	 * @tparam BUS Bus accessor class
	 * @tparam BUS_SELECTOR Opaque class that allows the bus overlay to access the device where the register is located (e.g. a bus address in I2C or a chip select with SPI)
	 * @tparam REG_ADDR Register offset inside the remote chip memory map
	 * @tparam T Type of the remote register
	 * @tparam DEFAULT_VAL Default value of the remote register when the remote chip undergoes a reset (e.g. power cycle)
	 */
	template<class BUS, class BUS_SELECTOR, class REG_ADDR, typename T, T DEFAULT_VAL>
	class RemoteRegister
	{
		RemoteRegister(){}

	public:
		typedef T register_value_type_t;

		static T get(void)
		{
			// return static_cast<T>(*(reinterpret_cast<T*>(REG_ADDR::value)));
			return BUS::template get<T>(BUS_SELECTOR, REG_ADDR);
		}

		static void set(const T val, const bool verify = false)
		{
			BUS::template set<T>(BUS_SELECTOR, REG_ADDR);
			if(verify)
			{
				if(get() != val)
					throw std::runtime_error("Remote value mismatch after write operation");
			}
		}

		static constexpr auto address = REG_ADDR::value;

		static constexpr T default_value(void) { return DEFAULT_VAL; } 

		static bool is_default(void) { return get() == default_value(); }
	};

	// template<class BUS, class ADDR, uint_fast32_t DEFAULT_VAL>
	// using Register8 = RemoteRegister<BUS, ADDR, uint8_t, DEFAULT_VAL>;
	// template<class BUS, class ADDR, uint_fast32_t DEFAULT_VAL>
	// using Register16 = RemoteRegister<BUS, ADDR, uint16_t, DEFAULT_VAL>;
	// template<class BUS, class ADDR, uint_fast32_t DEFAULT_VAL>
	// using Register32 = RemoteRegister<BUS, ADDR, uint32_t, DEFAULT_VAL>;
	// template<class BUS, class ADDR, uint_fast32_t DEFAULT_VAL>
	// using Register64 = RemoteRegister<BUS, ADDR, uint64_t, DEFAULT_VAL>;
	
} }