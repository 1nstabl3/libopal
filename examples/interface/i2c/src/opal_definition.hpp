
#pragma once

#include "hal/hal.hpp"

typedef opal::Hardware::Driver::NRF52840 SOC_t;

typedef opal::Hardware::Interface::GPIO::Port<0> PORT0_t;

typedef opal::Hardware::Interface::GPIO::Pin<26> PIN26_t;
typedef opal::Hardware::Interface::GPIO::Pin<27> PIN27_t;

typedef opal::Hardware::Interface::GPIO::Instance<SOC_t, PORT0_t, PIN26_t> GPIO0_26;
typedef opal::Hardware::Interface::GPIO::Instance<SOC_t, PORT0_t, PIN27_t> GPIO0_27;

typedef opal::Hardware::Interface::I2C::Instance<SOC_t, 0, GPIO0_26, GPIO0_27, 400000> I2C0;