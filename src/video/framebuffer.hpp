/**
 * @file i2c.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief I2C bus driver implementation
 * @version 0.1
 * @date 2020-11-15
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <stdint.h>

/* HAL Includes
 * ----------------------
 */

/* App Includes
 * ----------------------
 */

/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */
namespace opal { namespace Video { namespace Scene
{
    template<typename T>
    class RGBColor
    {
        public:
            RGBColor() : RGBColor(0,0,0) {}
            RGBColor(const T& red, const T& green, const T& blue) : r(red), g(green), b(blue) {}

            T r;
            T g;
            T b;
    };

    template<typename T>
    class Alpha
    {
        public:
            Alpha() : Alpha(0) {}
            Alpha(const T& alpha) : a(alpha) {}

            T a;
    };

    template<class COLOR, typename T>
    class Pixel
    {
        public:
            Pixel(){}

            COLOR       color;
            Alpha<T>    alpha;
    };

    template<class PIXEL>
	class Framebuffer
    {
        public:
            Framebuffer(){}

            void set_dimensions(const uint32_t w, const uint32_t h){}
    };

    template<class DisplayDriver>
    class Display
    {
        public:
            Display(){}

    };

    class WS28126b
    {
            WS28126b(){}
        public:

            static void set(const uint_fast8_t r, const uint_fast8_t g, const uint_fast8_t b)
            {

            }
    };

} } }