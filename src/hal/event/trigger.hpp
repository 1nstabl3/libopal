/**
 * @file trigger.hpp
 * @author Patrick Günzel (patrickguenzel@outlook.com)
 * @brief Event trigger abstraction class (e.g. GPIO Interrupt)
 * @version 0.1
 * @date 2020-12-29
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#pragma once

#include <cstdint>

namespace opal { namespace Hardware { namespace Event 
{
	enum class TRIGGER_TYPE : uint_fast8_t
	{
		MANUAL = 0,
		INTERRUPT,
		TIMER,
		SOFT_TIMER
	};	


	template< class TRIGGER_SOURCE >
	class Trigger
	{
		public: 
			Triggger(){}

        protected:
		    virtual void _trigger_handler(const TRIGGER_TYPE trig) = 0;

	};

} } }