/**
 * @file lsm6dsox.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief Driver implementation for the LSM6DSOX 6-axis IMU
 * @version 0.1
 * @date 2020-11-15
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <stdint.h>
#include <functional>

/* HAL Includes
 * ----------------------
 */
#include <zephyr.h>
#include <logging/log.h>
#include <device.h>
#include <drivers/sensor.h>

/* App Includes
 * ----------------------
 */
#include "register.hpp"
#include "field.hpp"

/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */

/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */
// LOG_MODULE_REGISTER(IMU, LOG_LEVEL_DBG);


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */
namespace opal { namespace Hardware { namespace Driver
{
	template< class BUS, class DEV_ADDR>
	class LSM6DSOX
	{
		template<uint_fast8_t REG_ADDR, uint_fast8_t DEF_VAL>
		using REG = opal::Hardware::Register8<BUS, DEV_ADDR, Interface::Address<RootAddress, REG_ADDR>, DEF_VAL>; 

		template<class REGISTER, uint_fast8_t OFFSET, uint_fast32_t BITLENGTH>
		using FLD = opal::Hardware::BitField<REGISTER, OFFSET, BITLENGTH>; 

		template<class REGISTER, uint_fast8_t OFFSET>
		using FLD1 = opal::Hardware::SingleBitField<REGISTER, OFFSET>; 

		LSM6DSOX(){}

	public:	// Register Definition
		

	public:	// stateless methods
		static bool check_who_am_i(void) 
		{ 
			return WHO_AM_I::is_default();
		}

		static void set_acc_odr(const ODR& new_ord)
		{
			ACC_ODR_FIELD::set(new_ord);
		}
	};

} } }