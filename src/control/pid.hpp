/**
 * @file pid.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief PID controller implementation
 * @version 0.1
 * @date 2020-07-28
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <stdint.h>

/* HAL Includes
 * ----------------------
 */

/* App Includes
 * ----------------------
 */


/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */
// LOG_MODULE_REGISTER(CONTROL, LOG_LEVEL_DBG);


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */

namespace opal { namespace Control { namespace Linear
{	
	class PIDController
	{
	private:
		/* data */
	public:
		PIDController(/* args */);
		~PIDController();
	};
} } }