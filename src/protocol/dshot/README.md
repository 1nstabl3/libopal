# DSHOT

A digital, PWM based, half-duplex serial protocol used for brushless motor driver communication.

## Encoding
Bit encoding is done by the pulse `HIGH` length in relation to the `LOW` pulse length. The overall wavelength (`HIGH` + `LOW`) is determined by the selected protocol speed:

| DSHOT | Baudrate | Wavelength | 
|-------|----------|------------|
| 150 | 150kbit/s | 6667ns |
| 300 | 300kbit/s | 3333ns |
| 600 | 600kbit/s | 1667ns |
| 1200 | 1200kbit/s | 833ns |

A shorter `HIGH` pulse encodes a `0`, a longer pulse a `1`. The overall timing requirements are not super strict. Different sources show slightly different numbers for the pulse widths. This allows for some amount of hardware deficites and noise.

### Logical 0

| DSHOT | HIGH Period | LOW Period | 
|-------|----------|------------|
| 150 | 2500ns | 4167ns |
| 300 | 1250ns | 2083ns |
| 600 | 625ns | 1042ns |
| 1200 | 312ns | 521ns |

### Logical 1

| DSHOT | HIGH Period | LOW Period | 
|-------|----------|------------|
| 150 | 5000ns | 1667ns |
| 300 | 2500ns | 833ns |
| 600 | 1250ns | 417ns |
| 1200 | 625ns | 208ns |

A mandatory pause between two frames is needed to signal a new frame although no hard restrictions are stated. The pause is approx. `2ms` in betaflight for DSHOT600 and around `14ms` for DSHOT150.

## Frame Design
A DSHOT frame consists of 16-bit

|    16 ... 6      |         5         |  4 ... 0 |
|------------------|-------------------|----------|
| Throttle/Command | Telemetry Request | Checksum |

The first 11 bits contain the value or command, most significant bit first. With the first `48` values beeing reserved for commands there are `2048-48=2000` values left for the motor throttle command.

The telemetry request bit indicates, that the FC requests telemetry over the **serial telemetry return channel**. This is not the bidirectional telemetry return discussed later.

The checksum is a 4-bit *XOR*. 

## Control Command Codes
The values `0` to `47` are reserved for command codes defined as following:

| Command | Code | Timing/Repetition Requirements | Description |
|---------|------|--------------------------------|-------------|
| MOTOR_STOP| 0 | ||
| BEEP1 | 1 | ||
| BEEP2 | 2 | ||
| BEEP3 | 3 | ||
| BEEP4 | 4 | ||
| BEEP5 | 5 | ||
| ESC_INFO | 6 | ||
| SPIN_DIRECTION_1 | 7 | ||
| SPIN_DIRECTION_2 | 8 | ||
| MODE_3D_OFF | 9 | ||
| MODE_3D_ON | 10 | ||
| SETTINGS_REQUEST | 11 | ||
| SAVE_SETTINGS | 12 | ||
| SPIN_DIRECTION_NORMAL | 20 | ||
| SPIN_DIRECTION_REVERSED | 21 | ||
| LED0_ON | 22 | ||
| LED1_ON | 23 | ||
| LED2_ON | 24 | ||
| LED3_ON | 25 | ||
| LED0_OFF | 26 | ||
| LED1_OFF | 27 | ||
| LED2_OFF | 28 | ||
| LED3_OFF | 29 | ||
| Audio_Stream mode on/Off	| 30 | ||
| Silent mode on/Off	| 31 | ||
| SIGNAL_LINE_TELEMETRY_DISABLE | 32 | ||
| SIGNAL_LINE_TELEMETRY_ENABLE | 33 | ||
| SIGNAL_LINE_CONTINUOUS_ERPM_TELEMETRY | 34 | ||
| SIGNAL_LINE_CONTINUOUS_ERPM_PERIOD_TELEMETRY | 35 | ||
| *NOT YET ASSIGNED* | 36 | ||
| *NOT YET ASSIGNED* | 37 | ||
| *NOT YET ASSIGNED* | 38 | ||
| *NOT YET ASSIGNED* | 39 | ||
| *NOT YET ASSIGNED* | 40 | ||
| *NOT YET ASSIGNED* | 41 | ||
| SIGNAL_LINE_TEMPERATURE_TELEMETRY | 42 | ||
| SIGNAL_LINE_VOLTAGE_TELEMETRY | 43 | ||
| SIGNAL_LINE_CURRENT_TELEMETRY | 44 | ||
| SIGNAL_LINE_CONSUMPTION_TELEMETRY | 45 | ||
| SIGNAL_LINE_ERPM_TELEMETRY | 46 | ||
| SIGNAL_LINE_ERPM_PERIOD_TELEMETRY | 47 | ||

## Protocol Flow
### Initialization
### Configuration
### Operation

## Further Reading
A non-exhausing list of further informations:
* [https://blck.mn/2016/11/dshot-the-new-kid-on-the-block/](https://blck.mn/2016/11/dshot-the-new-kid-on-the-block/)
* [https://github.com/betaflight/betaflight/pull/7264](https://github.com/betaflight/betaflight/pull/7264)
* [https://dmrlawson.co.uk/index.php/2017/12/04/dshot-in-the-dark/](https://dmrlawson.co.uk/index.php/2017/12/04/dshot-in-the-dark/)