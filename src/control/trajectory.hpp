/**
 * @file trajectory.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief Trajectory planning representation
 * @version 0.1
 * @date 2020-08-18
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <stdint.h>

/* HAL Includes
 * ----------------------
 */
#include <logging/log.h>

/* App Includes
 * ----------------------
 */


/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */
// LOG_MODULE_REGISTER(CONTROL, LOG_LEVEL_DBG);


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */

namespace opal { namespace Control
{
	class Trajectory
	{
	public:
		Trajectory();

	private:
		
	};
} }