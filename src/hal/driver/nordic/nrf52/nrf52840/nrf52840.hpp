/**
 * @file nrf52840.hpp
 * @author Patrick Günzel (patrickguenzel@outlook.com)
 * @brief Driver implementation for the Nordic Semiconductor nrf52840 SoC to be used by the opal HAL
 * @version 0.1
 * @date 2020-12-30
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#pragma once

#include <cstdint>
#include <type_traits>

#include "hal/driver/arm/v7e/armv7e-m4.hpp"
#include "hal/primitives/register.hpp"
#include "hal/primitives/address.hpp"
#include "hal/primitives/field.hpp"
#include "hal/interface/gpio.hpp"
#include "hal/interface/i2c.hpp"

namespace opal { namespace Hardware { namespace Driver
{
	class NRF52840 : public ARMv7eM4 
	{
			NRF52840(){};

			template<class ADDR_BASE, uint32_t REG_ADDR, uint32_t DEFAULT_VAL>
			using REG = Register<Address<ADDR_BASE, REG_ADDR>, uint32_t, DEFAULT_VAL>;

			template<class REGISTER, uint_fast8_t OFFSET, uint_fast32_t BITLENGTH>
			using FLD = opal::Hardware::BitField<REGISTER, OFFSET, BITLENGTH>; 

			template<class REGISTER, uint_fast8_t OFFSET>
			using FLD1 = opal::Hardware::SingleBitField<REGISTER, OFFSET>; 

		public:
			// #include "nrf52840_gpiote.map"
			template<class PORT, class PIN>
			class GPIOTE
			{
				GPIOTE(){}

				public:	// Register Definition
					typedef AddressSpace<0x50000000> PORT0_BASE;
					typedef AddressSpace<0x50000300> PORT1_BASE;

				/* select an AddressSpace based on the PORT value */
				typedef typename std::conditional<PORT::value == 0, PORT0_BASE, PORT1_BASE>::type PORT_INSTANCE;

					/*  OUT         0x504   Write GPIO port
						OUTSET      0x508   Set individual bits in GPIO port
						UTCLR       0x50C   Clear individual bits in GPIO port
						IN          0x510   Read GPIO port
						DIR         0x514   Direction of GPIO pins
						DIRSET      0x518   DIR set register
						DIRCLR      0x51C   DIR clear register
						LATCH       0x520   Latch register indicating what GPIO pins that have met the criteria set in the PIN_CNF[n].SENSEregisters
						DETECTMODE  0x524   Select between default DETECT signal behaviour and LDETECT mode
						PIN_CNF[0]  0x700   Configuration of GPIO pins
						PIN_CNF[1]  0x704   Configuration of GPIO pins
						...
						PIN_CNF[31] 0x77C   Configuration of GPIO pins
					 */
					typedef REG<PORT_INSTANCE, 0x504, 0x00>                             OUT;
					typedef FLD1<OUT, PIN::value> 			OUT_DRIVE_STATE;      		/**< Set to 1 if GPIO configured as output AND set to HIGH */
					typedef REG<PORT_INSTANCE, 0x508, 0x00>                             OUTSET;
					typedef FLD1<OUTSET, PIN::value> 		OUT_SET;					/**<  */
					typedef REG<PORT_INSTANCE, 0x50C, 0x00>                             UTCLR;
					typedef FLD1<UTCLR, PIN::value> 		OUT_CLEAR;					/**<  */
					typedef REG<PORT_INSTANCE, 0x510, 0x00>                             IN;
					typedef FLD1<IN, PIN::value> 			IN_STATE;					/**<  */
					typedef REG<PORT_INSTANCE, 0x514, 0x00>                             DIR;
					typedef FLD1<DIR, PIN::value> 			PIN_DIR;					/**<  */
					typedef REG<PORT_INSTANCE, 0x518, 0x00>                             DIRSET;
					typedef FLD1<DIRSET, PIN::value> 		PIN_DIR_SET;				/**<  */
					typedef REG<PORT_INSTANCE, 0x51C, 0x00>                             DIRCLR;
					typedef FLD1<DIRCLR, PIN::value> 		PIN_DIR_CLEAR;				/**<  */
					typedef REG<PORT_INSTANCE, 0x520, 0x00>                             LATCH;
					typedef FLD1<LATCH, PIN::value> 		PIN_LATCH_STATE;			/**<  */
					typedef REG<PORT_INSTANCE, 0x524, 0x00>                             DETECTMODE;
					typedef FLD1<DETECTMODE, PIN::value> 	PIN_DETECT_MODE;			/**<  */
					typedef REG<PORT_INSTANCE, 0x700 + (PIN::value * 0x4), 0x00000002>  PIN_CFG;
					typedef FLD1<PIN_CFG, 0> 				CFG_PIN_DIR;				/**<  */
					typedef FLD1<PIN_CFG, 1> 				CFG_PIN_BUFFERED;			/**<  */
					typedef FLD<PIN_CFG, 2, 2> 				CFG_PIN_PULL;				/**<  */
					typedef FLD<PIN_CFG, 8, 3> 				CFG_PIN_DRIVE;				/**<  */
					typedef FLD<PIN_CFG, 16, 2> 			CFG_PIN_SENSE;				/**<  */
			};
			
			template<class PORT, class PIN>
			class GPIODriver
			{
				GPIODriver(){};

				using PERIPHERAL = GPIOTE<PORT, PIN>;

				public:
					static void set(const bool val)
					{
						if(val)
							PERIPHERAL::OUT_SET::set(1);
						else
							PERIPHERAL::OUT_CLEAR::set(1);
					}

					static const bool get()
					{
						return PERIPHERAL::IN_STATE::get();
					}

					static void configure(	const Interface::GPIO::DIR	 			io_dir, 
											const Interface::GPIO::PULL 			io_pull, 
											const Interface::GPIO::IRQ 				io_irq, 
											const Interface::GPIO::DRIVE_STRENGTH 	io_pwr)
					{
						uint32_t cfg_reg_val = 0;
						cfg_reg_val |= PERIPHERAL::CFG_PIN_DIR::prepare(_map_from_direction(io_dir));
						cfg_reg_val |= PERIPHERAL::CFG_PIN_BUFFERED::prepare(1);	// no idea what it does and default is disconnected (low pass filter?!)
						cfg_reg_val |= PERIPHERAL::CFG_PIN_PULL::prepare(_map_from_pull(io_pull));
						cfg_reg_val |= PERIPHERAL::CFG_PIN_DRIVE::prepare(_map_from_drive_strength(io_pwr));
						cfg_reg_val |= PERIPHERAL::CFG_PIN_SENSE::prepare(_map_from_irq(io_irq));
						PERIPHERAL::PIN_CFG::set(cfg_reg_val);
					}

					static void get_configuration()
					{
						uint32_t 				cfg_reg_val = PERIPHERAL::PIN_CFG::get();
						Interface::GPIO::DIR 				io_dir 		= _map_to_direction(PERIPHERAL::CFG_PIN_DIR::extract(cfg_reg_val));
						Interface::GPIO::PULL 				io_pull 	= _map_to_pull(PERIPHERAL::CFG_PIN_PULL::extract(cfg_reg_val));
						Interface::GPIO::IRQ 				io_irq 		= _map_to_irq(PERIPHERAL::CFG_PIN_SENSE::extract(cfg_reg_val));
						Interface::GPIO::DRIVE_STRENGTH 	io_pwr 		= _map_to_drive_strength(PERIPHERAL::CFG_PIN_DRIVE::extract(cfg_reg_val));
					}

				private:
					static uint32_t _map_from_direction(const Interface::GPIO::DIR io_dir) { return static_cast<uint32_t>(io_dir); }
					static Interface::GPIO::DIR _map_to_direction(const uint32_t val) { return static_cast<Interface::GPIO::DIR>(val); }

					static uint32_t _map_from_pull(const Interface::GPIO::PULL io_pull) 
					{
						switch(io_pull)
						{
							case Interface::GPIO::PULL::UP:		return 3;
							case Interface::GPIO::PULL::DOWN: 	return 1;
							default:
							case Interface::GPIO::PULL::NONE: 	return 0;
						}
					}
					static Interface::GPIO::PULL _map_to_pull(const uint32_t val)
					{
						switch(val)
						{
							case 3: return Interface::GPIO::PULL::UP;
							case 1: return Interface::GPIO::PULL::DOWN;
							default:
							case 0:	return Interface::GPIO::PULL::NONE;
						}
					}

					static uint32_t _map_from_drive_strength(const Interface::GPIO::DRIVE_STRENGTH io_pwr)
					{
						return static_cast<uint32_t>(io_pwr);
					}

					static Interface::GPIO::DRIVE_STRENGTH _map_to_drive_strength(const uint32_t val)
					{
						return static_cast<Interface::GPIO::DRIVE_STRENGTH>(val);
					}

					static uint32_t _map_from_irq(const Interface::GPIO::IRQ io_irq)
					{
						switch(io_irq)
						{
							case Interface::GPIO::IRQ::HIGH_LEVEL:	return 2;
							case Interface::GPIO::IRQ::LOW_LEVEL: 	return 3;
							default:
							case Interface::GPIO::IRQ::NONE: 		return 0;
						}
					}
					static Interface::GPIO::IRQ _map_to_irq(const uint32_t val)
					{
						switch(val)
						{
							case 2: return Interface::GPIO::IRQ::HIGH_LEVEL;
							case 3: return Interface::GPIO::IRQ::LOW_LEVEL;
							default:
							case 0: return Interface::GPIO::IRQ::NONE;
						}
					}
			};

			template<class INSTANCE>
			class TWIM
			{
				TWIM(){};
				public:
					typedef AddressSpace<0x40003000> INST0_BASE;
					typedef AddressSpace<0x40004000> INST1_BASE;
					
					typedef typename std::conditional<INSTANCE::value == 0, INST0_BASE, INST1_BASE>::type BUS_INSTANCE;

					typedef REG<BUS_INSTANCE, 0x000, 0x00>  TASKS_STARTRX; 		// Start TWI receive sequence
					typedef REG<BUS_INSTANCE, 0x008, 0x00>  TASKS_STARTTX; 		// Start TWI transmit sequence
					typedef REG<BUS_INSTANCE, 0x014, 0x00>  TASKS_STOP; 		// Stop TWI transaction_ Must be issued while the TWI master is not suspended_
					typedef REG<BUS_INSTANCE, 0x01C, 0x00>  TASKS_SUSPEND; 		// Suspend TWI transaction
					typedef REG<BUS_INSTANCE, 0x020, 0x00>  TASKS_RESUME; 		// Resume TWI transaction
					typedef REG<BUS_INSTANCE, 0x104, 0x00>  EVENTS_STOPPED; 	// TWI stopped
					typedef REG<BUS_INSTANCE, 0x124, 0x00>  EVENTS_ERROR; 		// TWI error
					typedef REG<BUS_INSTANCE, 0x148, 0x00>  EVENTS_SUSPENDED; 	// Last byte has been sent out after the SUSPEND task has been issued, TWI traffic is now
					typedef REG<BUS_INSTANCE, 0x14C, 0x00>  EVENTS_RXSTARTED; 	// Receive sequence started
					typedef REG<BUS_INSTANCE, 0x150, 0x00>  EVENTS_TXSTARTED; 	// Transmit sequence started
					typedef REG<BUS_INSTANCE, 0x15C, 0x00>  EVENTS_LASTRX; 		// Byte boundary, starting to receive the last byte
					typedef REG<BUS_INSTANCE, 0x160, 0x00>  EVENTS_LASTTX; 		// Byte boundary, starting to transmit the last byte
					typedef REG<BUS_INSTANCE, 0x200, 0x00>  SHORTS; 			// Shortcut register
					typedef FLD1<SHORTS, 7> 	LASTTX_STARTRX;		// Shortcut between LASTTX event and STARTRX task
					typedef FLD1<SHORTS, 8> 	LASTTX_SUSPEND;		// Shortcut between LASTTX event and SUSPEND task
					typedef FLD1<SHORTS, 9> 	LASTTX_STOP;		// Shortcut between LASTTX event and STOP task
					typedef FLD1<SHORTS, 10> 	LASTRX_STARTTX;		// Shortcut between LASTRX event and STARTTX task
					typedef FLD1<SHORTS, 11> 	LASTRX_SUSPEND;		// Shortcut between LASTRX event and SUSPEND task
					typedef FLD1<SHORTS, 12> 	LASTRX_STOP;		// Shortcut between LASTRX event and STOP task
					typedef REG<BUS_INSTANCE, 0x300, 0x00>  INTEN; 				// Enable or disable interrupt
					typedef FLD1<INTEN, 1> 		INT_STOPPED;		// Enable or disable interrupt for STOPPED event
					typedef FLD1<INTEN, 9> 		INT_ERROR;			// Enable or disable interrupt for ERROR event
					typedef FLD1<INTEN, 18> 	INT_SUSPENDED;		// Enable or disable interrupt for SUSPENDED event
					typedef FLD1<INTEN, 19>		INT_RXSTARTED;		// Enable or disable interrupt for RXSTARTED event
					typedef FLD1<INTEN, 20>		INT_TXSTARTED;		// Enable or disable interrupt for TXSTARTED event
					typedef FLD1<INTEN, 23>		INT_LASTRX;			// Enable or disable interrupt for LASTRX event
					typedef FLD1<INTEN, 24>		INT_LASTTX;			// Enable or disable interrupt for LASTTX event
					typedef REG<BUS_INSTANCE, 0x304, 0x00>  INTENSET; 			// Enable interrupt
					typedef FLD1<INTENSET, 1> 	INTSET_STOPPED;		// Enable or disable interrupt for STOPPED event
					typedef FLD1<INTENSET, 9> 	INTSET_ERROR;			// Enable interrupt for ERROR event
					typedef FLD1<INTENSET, 18> 	INTSET_SUSPENDED;		// Enable interrupt for SUSPENDED event
					typedef FLD1<INTENSET, 19>	INTSET_RXSTARTED;		// Enable interrupt for RXSTARTED event
					typedef FLD1<INTENSET, 20>	INTSET_TXSTARTED;		// Enable interrupt for TXSTARTED event
					typedef FLD1<INTENSET, 23>	INTSET_LASTRX;			// Enable interrupt for LASTRX event
					typedef FLD1<INTENSET, 24>	INTSET_LASTTX;			// Enable interrupt for LASTTX event
					typedef REG<BUS_INSTANCE, 0x308, 0x00>  INTENCLR; 			// Disable interrupt
					typedef FLD1<INTENCLR, 1> 	INTCLR_STOPPED;			// Disable or disable interrupt for STOPPED event
					typedef FLD1<INTENCLR, 9> 	INTCLR_ERROR;			// Disable interrupt for ERROR event
					typedef FLD1<INTENCLR, 18> 	INTCLR_SUSPENDED;		// Disable interrupt for SUSPENDED event
					typedef FLD1<INTENCLR, 19>	INTCLR_RXSTARTED;		// Disable interrupt for RXSTARTED event
					typedef FLD1<INTENCLR, 20>	INTCLR_TXSTARTED;		// Disable interrupt for TXSTARTED event
					typedef FLD1<INTENCLR, 23>	INTCLR_LASTRX;			// Disable interrupt for LASTRX event
					typedef FLD1<INTENCLR, 24>	INTCLR_LASTTX;			// Disable interrupt for LASTTX event
					typedef REG<BUS_INSTANCE, 0x4C4, 0x00>  ERRORSRC; 			// Error source
					typedef FLD1<ERRORSRC, 0>	ERRSRC_OVERRUN;			// A new byte was received before previous byte got transferred into RXD buffer. (Previous data is lost)
					typedef FLD1<ERRORSRC, 1>	ERRSRC_ANACK;			// NACK received after sending the address (write '1' to clear)
					typedef FLD1<ERRORSRC, 2>	ERRSRC_DNACK;			// NACK received after sending a data byte (write '1' to clear)
					typedef REG<BUS_INSTANCE, 0x500, 0x00>  ENABLE; 			// Enable TWIM
					typedef FLD<ENABLE, 0, 4> 	EN;						// write 6 to enable or 0 to disable
					typedef REG<BUS_INSTANCE, 0x508, 0x00>  PSEL_SCL; 			// Pin select for SCL signal
					typedef FLD<PSEL_SCL, 0, 5> 	SCL_PIN;			// Pin number for SCL PIN
					typedef FLD<PSEL_SCL, 5, 1> 	SCL_PORT;			// PORT number for SCL PIN
					typedef FLD<PSEL_SCL, 31, 1> 	SCL_CONNECT;		// Connect pin to peripheral (1=disconnected, 0=connected)
					typedef REG<BUS_INSTANCE, 0x50C, 0x00>  PSEL_SDA; 			// Pin select for SDA signal
					typedef FLD<PSEL_SDA, 0, 5> 	SDA_PIN;			// Pin number for SDA PIN
					typedef FLD<PSEL_SDA, 5, 1> 	SDA_PORT;			// PORT number for SDA PIN
					typedef FLD<PSEL_SDA, 31, 1> 	SDA_CONNECT;		// Connect pin to peripheral (1=disconnected, 0=connected
					typedef REG<BUS_INSTANCE, 0x524, 0x04000000>  FREQUENCY; 			// TWI frequency. Accuracy depends on the HFCLK source selected_
					typedef REG<BUS_INSTANCE, 0x534, 0x00>  RXD_PTR; 			// Data pointer
					typedef REG<BUS_INSTANCE, 0x538, 0x00>  RXD_MAXCNT; 		// Maximum number of bytes in receive buffer
					typedef FLD<RXD_MAXCNT, 0, 16> 	RXD_MAXCNT_FLD; 
					typedef REG<BUS_INSTANCE, 0x53C, 0x00>  RXD_AMOUNT; 		// Number of bytes transferred in the last transaction
					typedef FLD<RXD_AMOUNT, 0, 16> 	RXD_AMOUNT_FLD; 
					typedef REG<BUS_INSTANCE, 0x540, 0x00>  RXD_LIST; 			// EasyDMA list type
					typedef FLD<RXD_LIST, 0, 3> 	RXD_USE_LIST; 
					typedef REG<BUS_INSTANCE, 0x544, 0x00>  TXD_PTR; 			// Data pointer
					typedef REG<BUS_INSTANCE, 0x548, 0x00>  TXD_MAXCNT; 		// Maximum number of bytes in transmit buffer
					typedef FLD<TXD_MAXCNT, 0, 16> 	TXD_MAXCNT_FLD; 
					typedef REG<BUS_INSTANCE, 0x54C, 0x00>  TXD_AMOUNT; 		// Number of bytes transferred in the last transaction suspended_
					typedef FLD<TXD_AMOUNT, 0, 16> 	TXD_AMOUNT_FLD; 
					typedef REG<BUS_INSTANCE, 0x550, 0x00>  TXD_LIST; 			// EasyDMA list type
					typedef FLD<TXD_LIST, 0, 3> 	TXD_USE_LIST; 
					typedef REG<BUS_INSTANCE, 0x588, 0x00>  ADDRESS; 			// Address used in the TWI transfer
					typedef FLD<ADDRESS, 0, 7> 	ADDRESS_FLD; 
			};

			template<class INST>
			class I2CDriver
			{
				I2CDriver(){};
				
				using PERIPHERAL = TWIM<INST>;

				public:
					template<class SDA, class SCL>
					static void set_pins()
					{
						PERIPHERAL::PSEL_SCL::set(0);
						PERIPHERAL::PSEL_SDA::set(0);
					}

					template<typename T, class ADDR, class REG_ADDR>
					static void set(const T& val)
					{
						PERIPHERAL::ADDRESS_FLD::set(ADDR::value);
					}

				private:
					static uint32_t _map_from_baud(const Interface::I2C::BAUD_RATE baud)
					{
						switch(baud)
						{
							case Interface::I2C::BAUD_RATE::B400KBPS: return 0x06400000;
							case Interface::I2C::BAUD_RATE::B100KBPS: return 0x01980000
							case Interface::I2C::BAUD_RATE::B250KBPS: 
							default: return 0x04000000;
						}
					}

					static const Interface::I2C::BAUD_RATE _map_to_baud(uint32_t val)
					{
						switch(val)
						{
							case 0x06400000: return Interface::I2C::BAUD_RATE::B400KBPS;
							case 0x01980000: return Interface::I2C::BAUD_RATE::B100KBPS;
							case 0x04000000: 
							default: return Interface::I2C::BAUD_RATE::B250KBPS;
						}
					}
			};
	};

} } }