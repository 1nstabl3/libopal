/**
 * @file main.cpp
 * @author Patrick Günzel (patrickguenzel@outlook.com)
 * @brief Demonstation sample for using the I2C interface module
 * @version 0.1
 * @date 2021-01-10
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <zephyr.h>
#include <logging/log.h>
#include <sys/printk.h>


#include "opal_definition.hpp"

using namespace opal::Hardware;
using namespace opal::Hardware::Interface;

typedef opal::Hardware::RemoteRegister<I2C0, Interface::Address<RootAddress, 0x6a>, Interface::Address<RootAddress, 0x0F>, 0x6C> WHOAMI; 

void main(void)
{

    printk("LSM6DSOx@0x6a WHOAMI: 0x%x (is_default=%d)\n", WHOAMI::get(), WHOAMI::is_default());

    while(true)
    {
    //     GPIO0_0::set(1);
    //     k_sleep(K_SECONDS(1));
    //     GPIO0_0::set(0);
        k_sleep(K_SECONDS(1));
        // printk(".\n");
    }
}
