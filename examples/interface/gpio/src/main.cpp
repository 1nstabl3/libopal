/**
 * @file main.cpp
 * @author Patrick Günzel (patrickguenzel@outlook.com)
 * @brief Demonstation sample for using the GPIO interface module
 * @version 0.1
 * @date 2021-01-10
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <zephyr.h>
#include <logging/log.h>
#include <sys/printk.h>


#include "opal_definition.hpp"

using namespace opal::Hardware;
using namespace opal::Hardware::Interface;

using PWR_3V8_EN    = GPIO1_2;
using BG95_PWR      = GPIO0_4;

//LOG_MODULE_REGISTER(APP, LOG_LEVEL_DBG);

void main(void)
{
    printk("\nConfiguring GPIO P%d.%d\n", PWR_3V8_EN::port_num(), PWR_3V8_EN::pin_num());
    PWR_3V8_EN::configure(	GPIO::DIR::OUTPUT, 
                            GPIO::PULL::DOWN, 
                            GPIO::IRQ::NONE, 
                            GPIO::DRIVE_STRENGTH::NORMAL);
    
    
    printk("\nConfiguring GPIO P%d.%d\n", BG95_PWR::port_num(), BG95_PWR::pin_num());
    BG95_PWR::configure(	GPIO::DIR::OUTPUT, 
                            GPIO::PULL::UP, 
                            GPIO::IRQ::NONE, 
                            GPIO::DRIVE_STRENGTH::NORMAL_LOW_DISONNECT_HIGH);
    BG95_PWR::set(1);
    
    
    
    
    
    constexpr int lvl = 1;
    printk("\nSetting GPIO P%d.%d = %d\n", PWR_3V8_EN::port_num(), PWR_3V8_EN::pin_num(), lvl);
    PWR_3V8_EN::set(lvl);
   

    printk("Toggeling BG95 power line\n");
    k_sleep(K_SECONDS(1));
    BG95_PWR::set(0);
    k_sleep(K_MSEC(700));
    BG95_PWR::set(1);


    printk("\nSuspend\n");
    while(true)
    {
    //     GPIO0_0::set(1);
    //     k_sleep(K_SECONDS(1));
    //     GPIO0_0::set(0);
        k_sleep(K_SECONDS(1));
        // printk(".\n");
    }
}
