#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include <cstdint>

#include "doctest.h"
#include "hal/primitives/field.hpp"

static uint8_t register_value = 0b00000110;

class RegisterDummy
{
        RegisterDummy() /*: value(init_val)*/ {};
    public:
		typedef uint8_t register_value_type_t;

        static register_value_type_t get(void)
		{
			return register_value;
		}

		static void set(const register_value_type_t val)
		{
			std::cout << std::to_string(val) << std::endl;
            register_value = val;
		}

		static constexpr register_value_type_t default_value(void) { return 123; } 

		static bool is_default(void) { return get() == default_value(); }
};

/* ##########################################################################################
 * ##########################################################################################
 */

TEST_SUITE("Fields") {
    TEST_CASE(  "MASK"
                * doctest::description("shouldn't take more than 500ms")
                * doctest::timeout(0.5)) 
    {
        typedef opal::Hardware::BitField<RegisterDummy, 2, 2> obj0_t;
        CHECK(obj0_t::MASK() == (0b11 << 2));
        CHECK(obj0_t::NMASK() == 0b11110011);
    }

/* ##########################################################################################
 */

    TEST_CASE(  "extract"
                * doctest::description("shouldn't take more than 500ms")
                * doctest::timeout(0.5)) 
    {
        typedef opal::Hardware::BitField<RegisterDummy, 2, 2> obj0_t;
        CHECK(obj0_t::extract(0) == 0);
        CHECK(obj0_t::extract(1 << 2) == 0b01);
        CHECK(obj0_t::extract(0b11 << 2) == 0b11);
        CHECK(obj0_t::extract(0b11 << 3) == 0b10);
        CHECK(obj0_t::extract(0b11 << 4) == 0b00);
    }

/* ##########################################################################################
 */

    TEST_CASE(  "prepare"
                * doctest::description("shouldn't take more than 500ms")
                * doctest::timeout(0.5)) 
    {
        typedef opal::Hardware::BitField<RegisterDummy, 2, 2> obj0_t;
        CHECK(obj0_t::prepare(0) == 0);
        CHECK(obj0_t::prepare(1) == 0b0100);
        CHECK(obj0_t::prepare(2) == 0b1000);
        CHECK(obj0_t::prepare(3) == 0b1100);
        CHECK(obj0_t::prepare(4) == 0b0000);
    }

/* ##########################################################################################
 */

    TEST_CASE(  "get"
                * doctest::description("shouldn't take more than 500ms")
                * doctest::timeout(0.5)) 
    {
        typedef opal::Hardware::BitField<RegisterDummy, 2, 2> obj0_t;
        CHECK(obj0_t::get() == 0b01);
    }  

/* ##########################################################################################
 */
   
    TEST_CASE(  "set"
                * doctest::description("shouldn't take more than 500ms")
                * doctest::timeout(0.5)) 
    {
        typedef opal::Hardware::BitField<RegisterDummy, 2, 2> obj0_t;
        obj0_t::set(3);
        CHECK(obj0_t::get() == 0b11);
        CHECK(RegisterDummy::get() == 0b1110);
    }   
}