#pragma once

#include <cstdint>

namespace opal { namespace Hardware 
{ 
	template<class REGISTER, uint_fast8_t OFFSET, uint_fast32_t BITLENGTH>
	class BitField
	{
		BitField(){}

		typedef typename REGISTER::register_value_type_t value_type_t;


	public:
		static constexpr value_type_t MASK() {return ((1 << BITLENGTH) - 1) << OFFSET; };
		static constexpr value_type_t NMASK() { return ~MASK(); };
		// static constexpr uint_fast8_t OFFSET;

		template<typename T = value_type_t>
		static T get()
		{
			return static_cast<T>(extract(REGISTER::get()));
		}

		static void set(const value_type_t& val)
		{
			value_type_t tmp_val = REGISTER::get();
			tmp_val = 0 | (tmp_val & NMASK()) | prepare(val);
			REGISTER::set(tmp_val);
		}

		static value_type_t prepare(const value_type_t& val)
		{
			return ((val << OFFSET) & MASK());
		}

		static value_type_t extract(const value_type_t& input)
		{
			return (input & MASK()) >> OFFSET;
		}
	};

	/* -- Convenience Types -- */
	template<class REGISTER, uint_fast8_t OFFSET>
	using SingleBitField = BitField<REGISTER, OFFSET, 1>;
} }