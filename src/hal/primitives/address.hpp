/**
 * @file address.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief Address abstraction class
 * @version 0.1
 * @date 2020-11-15
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <cstdint>
#if __cplusplus >= 202002L
#include <concepts>
#endif // c++20

/* HAL Includes
 * ----------------------
 */

/* App Includes
 * ----------------------
 */


/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */
namespace opal { namespace Hardware 
{
	template< std::uintptr_t ADDR_SPACE_BASE_VAL >
	class AddressSpace
	{
		AddressSpace(){};
		public:
			static constexpr std::uintptr_t value = ADDR_SPACE_BASE_VAL; 
	};

	typedef AddressSpace<0x00> AddressSpaceRoot;

	template< class BASE, std::uintptr_t OFFSET >
	class Address
	{
		Address(){}
		public:
			
			static constexpr std::uintptr_t value = BASE::value + OFFSET; 
	};
	
	#if __cplusplus >= 202002L
	template< class CANDIDATE >
	concept IsAnAddress = std::is_base_of<CANDIDATE, template< class BASE, std::uintptr_t VALUE > Address<BASE, OFFSET> >::value;
	#endif // c++20
} } 