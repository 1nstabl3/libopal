/**
 * @file shiftregister.hpp
 * @author Patrick Günzel (patrickguenzel@outlook.com)
 * @brief Shift Register implementation to be used with a Encoding and Hardware Interface
 * @version 0.1
 * @date 2021-03-13
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <stdint.h>

/* HAL Includes
 * ----------------------
 */

/* App Includes
 * ----------------------
 */

/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */
namespace opal { namespace Hardware { namespace Interface
{
    template<class ENCODER, class INTERFACE>
    class ShiftRegister
    {
            ShiftRegister(){}
        public:

            template<class T>
            static void push_in(const T& data)
            {
                INTERFACE::send(ENCODER::encode(data));
            }            
    };

} } }