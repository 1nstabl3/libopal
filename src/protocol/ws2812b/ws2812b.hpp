#ifndef DEF_WS2812B_H
#define DEF_WS2812B_H

#include <cstdint>

/* Idea
 * --------
 * Generate (compile time) array of bytes with the correct pixel-channel-values
 * Define Bit-timing-patterns and use it to encode bytearray
 * 
 *
 */

namespace opal { namespace Protocol { namespace WS2812 
{
    constexpr uint_fast16_t HIGH_PULSE_PERIOD_NS(const bool bitval)
	{
		if(bitval)
            return 700;
        else
            return 350;
	}

    constexpr uint_fast16_t LOW_PULSE_PERIOD_NS(const bool bitval)
	{
		if(bitval)
            return 600;
        else
            return 800;
	} 	

    constexpr uint_fast16_t RESET_PULSE_PERIOD_NS = 50'000;

    // 8bit per Channel (24bit per Frame)
    // Green-Red-Blue
    // G7 .. G0 R7 .. R0 B7 .. B0
    // F1-F2-F3-F4 RESET F1-F2-F3-F4 RESET ....
    // 400kbps
    // 300ns daisy-chain transmission delay (D1->D2)

   class Frame
    {
            Frame(){}
        public:
            static constexpr int8_t BITLENGTH = 24;
            
            typedef struct Payload
            {
                uint8_t red;
                uint8_t green;
                uint8_t blue;

                Payload() : Payload(0,0,0) {}
                Payload(const uint8_t r, const uint8_t g, const uint8_t b) : red(r), green(g), blue(b) {}
            }Payload_t;

            template<typename T>
            static T encode(const uint8_t red, const uint8_t green, const uint8_t blue)
            {
                return green << 16 | red << 8 | blue;
            }

            template<typename T>
            static Payload_t decode(const T data)
            {
                return Payload_t((data >> 8) & 0xff, (data >> 16) & 0xff, (data) & 0xff);
            }
    };

}}}

#endif