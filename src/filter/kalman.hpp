/**
 * @file kalman.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief Kalman Filter Implementation based on Eigen matrix library
 * @version 0.1
 * @date 2020-05-24
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <stdint.h>

/* HAL Includes
 * ----------------------
 */
#include <logging/log.h>

/* App Includes
 * ----------------------
 */
#include <Eigen/Dense>

/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */
// LOG_MODULE_REGISTER(FILTER, LOG_LEVEL_DBG);


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */

namespace opal { namespace Filter { namespace Kalman
{	
	typedef struct
	{
		unsigned int additions;
		unsigned int multiplications;
		unsigned int divisions;
	}comp_effort_t;

	template<typename PRECISION, unsigned int STATE_DIM, unsigned int MEASUREMENT_DIM, unsigned int INPUT_DIM>
	class Conventional
	{
		typedef Eigen::Matrix<PRECISION, STATE_DIM, STATE_DIM> 				MATss;
		typedef Eigen::Matrix<PRECISION, STATE_DIM, MEASUREMENT_DIM> 		MATsm;
		typedef Eigen::Matrix<PRECISION, MEASUREMENT_DIM, MEASUREMENT_DIM> 	MATmm;
		typedef Eigen::Matrix<PRECISION, STATE_DIM, INPUT_DIM> 				MATsi;
		typedef Eigen::Matrix<PRECISION, MEASUREMENT_DIM, STATE_DIM> 		MATms;

		typedef Eigen::Vector<PRECISION, STATE_DIM> 		VECs;
		typedef Eigen::Vector<PRECISION, MEASUREMENT_DIM>	VECm;
		typedef Eigen::Vector<PRECISION, INPUT_DIM> 		VECi;

		public:
			Conventional() 
			{};

			Conventional(	const MATss& A_new,
							const MATss& Q_new,
							const MATss& L_new,
							const MATmm& R_new,
							const MATsi& G_new,
							const MATms& H_new ) : A(A_new), Q(Q_new), L(L_new), R(R_new), G(G_new), H(H_new)
			{};

			void init(	const VECs& z0, 
						const MATss& P0)
			{
				z = z0;
				P = P0;
				t = 0;
			}

			const VECs& calc_synchronous(const VECi& u, const VECm& s)
			{
				predict(u);
				correct(s);
				t++;
				return z;
			}

			constexpr unsigned int get_needed_additions() const
			{
				return 	((3.0f/2.0f * std::pow(STATE_DIM, 3)) + 
						(0.5f * std::pow(STATE_DIM, 2)*((3*MEASUREMENT_DIM)+INPUT_DIM-1)) + 
						(0.5f*STATE_DIM*((5*MEASUREMENT_DIM)+INPUT_DIM-2) ));
			}

			constexpr unsigned int get_needed_multiplications() const
			{
				return 	((3.0f/2.0f * std::pow(STATE_DIM, 3)) + 
						(0.5f * std::pow(STATE_DIM, 2)*((3*MEASUREMENT_DIM)+INPUT_DIM+3)) + 
						(3.0f/2.0f*STATE_DIM*((3*MEASUREMENT_DIM)+INPUT_DIM) ));
			}

			constexpr unsigned int get_needed_divisions() const
			{
				return 	MEASUREMENT_DIM;
			}

			constexpr comp_effort_t get_comp_effort() const
			{
				return { get_needed_additions(), get_needed_multiplications(), get_needed_divisions()};
			}
		private:
			void predict(const VECi& u)
			{
				z = ( A * z ) + ( G * u );
				P = ( A * P * A.transpose() ) + ( L * Q * L.transpose() );
			}

			void correct(const VECm& s)
			{
				auto PHt = P * H.transpose();
				K = PHt * ( ( H * PHt )  + R).inverse();
				z = z + ( K * (s - ( H * z )));
				P = (MATss::Identity() - (K * H)) * P;
			}

		public:
			void set_state_model(const MATss& model) { A = model; }
			void set_process_noise_tranformation_matrix(const MATss& model) { L = model; }
			void set_process_noise_model(const MATss& model) { Q = model; }
			void set_measurement_noise_model(const MATmm& model) { R = model; }
			void set_disturbance_model(const MATsi& model) { G = model; }
			void set_observer_model(const MATms& model) { H = model; }

		private:
			Eigen::Vector<PRECISION, STATE_DIM> 						z;	// observation state vector
			Eigen::Matrix<PRECISION, STATE_DIM, STATE_DIM> 				A;	// state transition matrix
			Eigen::Matrix<PRECISION, STATE_DIM, MEASUREMENT_DIM> 		K;	// measurement to state transformation (Kalman Gain)
			Eigen::Matrix<PRECISION, STATE_DIM, STATE_DIM> 				P;	// state covariance matrix
			Eigen::Matrix<PRECISION, STATE_DIM, STATE_DIM> 				L;	// state covariance matrix
			Eigen::Matrix<PRECISION, STATE_DIM, STATE_DIM> 				Q;	// Process noise
			Eigen::Matrix<PRECISION, MEASUREMENT_DIM, MEASUREMENT_DIM>	R;	// measurement noise covariance matrix
			Eigen::Matrix<PRECISION, STATE_DIM, INPUT_DIM> 				G;	// input to state transformation (control-input model or deterministic disturbance/error model)
			Eigen::Matrix<PRECISION, MEASUREMENT_DIM, STATE_DIM>		H;	// observer matrix (observer to measurement transformation)

			unsigned int t;	// calculation time step;
	};

	// template<unsigned int STATE_DIM, unsigned int MEASUREMENT_DIM>
	// using Conventionali = Conventional<double, STATE_DIM, MEASUREMENT_DIM>;	// Integer Version
	// template<unsigned int STATE_DIM, unsigned int MEASUREMENT_DIM>
	// using Conventionalf = Conventional<float, STATE_DIM, MEASUREMENT_DIM>;	// Float (single-precision) Version
	// template<unsigned int STATE_DIM, unsigned int MEASUREMENT_DIM>
	// using Conventionald = Conventional<double, STATE_DIM, MEASUREMENT_DIM>;	// Double (double-precision) Version

	class NonLinear
	{
		NonLinear();
	};

	class Unscented
	{
	private:
		/* data */
	public:
		Unscented(/* args */);
		~Unscented();
	};
	
	class SqareRootUnscented
	{
	private:
		/* data */
	public:
		SqareRootUnscented(/* args */);
		~SqareRootUnscented();
	};
		
} } }