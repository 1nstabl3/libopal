/**
 * @file dshot.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief DShot protocol implementation
 * @version 0.1
 * @date 2020-08-23
 * 
 * [https://blck.mn/2016/11/dshot-the-new-kid-on-the-block/](https://blck.mn/2016/11/dshot-the-new-kid-on-the-block/)
 * [https://github.com/betaflight/betaflight/pull/7264](https://github.com/betaflight/betaflight/pull/7264)
 * [https://dmrlawson.co.uk/index.php/2017/12/04/dshot-in-the-dark/](https://dmrlawson.co.uk/index.php/2017/12/04/dshot-in-the-dark/)
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <stdint.h>

/* HAL Includes
 * ----------------------
 */

/* App Includes
 * ----------------------
 */


namespace opal { namespace Protocol { namespace DShot {

/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */
	enum class DSHOT : uint_fast16_t
	{
		D150 = 150,
		D300 = 300,
		D600 = 600,
		D1200 = 1200
	};

	constexpr uint_fast32_t NS_PER_SEC = 1'000'000'000; 

	constexpr uint_fast16_t TOTAL_TIMING_PERIOD_NS(const DSHOT mode)
	{ return NS_PER_SEC/(static_cast<int>(mode) * 1000); }

	constexpr uint_fast16_t ZERO_BIT_HIGH_PULSE_PERIOD_NS(const DSHOT mode)
	{
		switch(mode)
		{
			case DSHOT::D150: return 2500;
			case DSHOT::D300: return 1250;
			case DSHOT::D600: return 625;
			case DSHOT::D1200: return 312;
		}
	} 	

	constexpr uint_fast16_t ONE_BIT_HIGH_PULSE_PERIOD_NS(const DSHOT mode)
	{
		switch(mode)
		{
			case DSHOT::D150: return 5000;
			case DSHOT::D300: return 2500;
			case DSHOT::D600: return 1250;
			case DSHOT::D1200: return 625;
		}
	}

	constexpr uint_fast16_t ZERO_BIT_LOW_PULSE_PERIOD_NS(const DSHOT mode)
	{ return TOTAL_TIMING_PERIOD_NS(mode) - ZERO_BIT_HIGH_PULSE_PERIOD_NS(mode); }

	constexpr uint_fast16_t ONE_BIT_LOW_PULSE_PERIOD_NS(const DSHOT mode)
	{ return TOTAL_TIMING_PERIOD_NS(mode) - ONE_BIT_HIGH_PULSE_PERIOD_NS(mode); }

	/* DSHOT Protocol
		* 
		* 16-bit frame:
		* 
		* |    16 ... 5      |         5         |  4 ... 0 |
		* |------------------|-------------------|----------|
		* | Throttle/Command | Telemetry Request | Checksum |
		*/
	constexpr uint_fast16_t 	VALUE_BITMASK = 0b0000'0111'1111'1111;
	constexpr uint_fast8_t 		VALUE_SHIFT 	= 5u;
	constexpr uint_fast8_t 		TELEMETRY_BITMASK = 0x01;
	constexpr uint_fast8_t 		TELEMETRY_SHIFT 	= 4u;
	constexpr uint_fast8_t 		CHECKSUM_BITMASK = 0b00001111;
	
	enum class COMMAND : uint_fast16_t
	{
		MOTOR_STOP 										= 0,	// Currently not implemented
		BEEP1 											= 1, 	// Wait at least length of beep (260ms) before next command
		BEEP2 											= 2, 	// Wait at least length of beep (260ms) before next command
		BEEP3 											= 3, 	// Wait at least length of beep (280ms) before next command
		BEEP4 											= 4, 	// Wait at least length of beep (280ms) before next command
		BEEP5 											= 5, 	// Wait at least length of beep (1020ms) before next command
		ESC_INFO 										= 6,  	// Wait at least 12ms before next command
		SPIN_DIRECTION_1 								= 7, 	// Need 6x, no wait required
		SPIN_DIRECTION_2 								= 8, 	// Need 6x, no wait required
		MODE_3D_OFF 									= 9, 	// Need 6x, no wait required
		MODE_3D_ON 										= 10,  	// Need 6x, no wait required
		SETTINGS_REQUEST 								= 11,  	// Currently not implemented
		SAVE_SETTINGS 									= 12,  	// Need 6x, wait at least 35ms before next command
		SPIN_DIRECTION_NORMAL 							= 20, 	// Need 6x, no wait required
		SPIN_DIRECTION_REVERSED 						= 21, 	// Need 6x, no wait required
		LED0_ON 										= 22, 	// No wait required
		LED1_ON 										= 23, 	// No wait required
		LED2_ON 										= 24, 	// No wait required
		LED3_ON 										= 25, 	// No wait required
		LED0_OFF 										= 26, 	// No wait required
		LED1_OFF 										= 27, 	// No wait required
		LED2_OFF 										= 28, 	// No wait required
		LED3_OFF 										= 29, 	// No wait required
		//Audio_Stream = 30, mode on/Off		      			// Currently not implemented
		//Silent = 31, Mode on/Off			      				// Currently not implemented
		SIGNAL_LINE_TELEMETRY_DISABLE 					= 32,   // Need 6x, no wait required. Disables commands 42 to 47
		SIGNAL_LINE_TELEMETRY_ENABLE 					= 33,   // Need 6x, no wait required. Enables commands 42 to 47
		SIGNAL_LINE_CONTINUOUS_ERPM_TELEMETRY 			= 34,   // Need 6x, no wait required. Enables commands 42 to 47, and sends erpm if normal Dshot frame
		SIGNAL_LINE_CONTINUOUS_ERPM_PERIOD_TELEMETRY 	= 35,   // Need 6x, no wait required. Enables commands 42 to 47, and sends erpm period if normal Dshot frame
		//Commands above are only executed when motors are stopped
		//36 // Not yet assigned 
		//37 // Not yet assigned 
		//38 // Not yet assigned 
		//39 // Not yet assigned 
		//40 // Not yet assigned 
		//41 // Not yet assigned 
		//Commands below are executed at any time
		SIGNAL_LINE_TEMPERATURE_TELEMETRY 				= 42,   // No wait required 
		SIGNAL_LINE_VOLTAGE_TELEMETRY 					= 43,   // No wait required 
		SIGNAL_LINE_CURRENT_TELEMETRY 					= 44,   // No wait required 
		SIGNAL_LINE_CONSUMPTION_TELEMETRY 				= 45,   // No wait required 
		SIGNAL_LINE_ERPM_TELEMETRY 						= 46,   // No wait required 
		SIGNAL_LINE_ERPM_PERIOD_TELEMETRY 				= 47,    // No wait required 
	};

	typedef struct 
	{
		union
		{
			COMMAND 		cmd;
			int_fast16_t 	value;
		};
		bool isCommand;
		bool telemetryRequested;		
	}decodeResult_t;
	

/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */

	class ValueFrame
	{
		ValueFrame(){}

		static constexpr uint_fast8_t VALUE_OFFSET_VALUE = 47;

	public: 
		static uint16_t encode(const uint_fast16_t value, const bool request_telemetry = false) 
		{ return _pack( (((value + VALUE_OFFSET_VALUE) & VALUE_BITMASK) << 1) | ((request_telemetry?1:0) & TELEMETRY_BITMASK) ); }

		static int_fast16_t decode(const uint16_t frame) 
		{ 
			if( (frame & CHECKSUM_BITMASK) != _csm(frame >> TELEMETRY_SHIFT) )
				return (-VALUE_OFFSET_VALUE - 1);
			else
				return (frame >> VALUE_SHIFT) - VALUE_OFFSET_VALUE;
		}

		static decodeResult_t decode_full(const uint16_t frame)
		{
			decodeResult_t result;
			auto val = decode(frame);
			if((val >= 0) || (val < (-VALUE_OFFSET_VALUE)))
			{
				result.isCommand = false;
				result.value = val;
			}
			else// if (val >= (-VALUE_OFFSET_VALUE))
			{
				result.isCommand = true;
				result.cmd = static_cast<COMMAND>(-val);
			}
			
			result.telemetryRequested = (((frame >> TELEMETRY_SHIFT) & TELEMETRY_BITMASK) == 1) ? true : false;
			return result;
		}

	private:
		static uint16_t _pack(const uint_fast16_t packet)
		{ return (packet << TELEMETRY_SHIFT) | _csm(packet); }

		static uint16_t _csm(const uint_fast16_t packet)
		{ return ((packet ^ (packet >> 4) ^ (packet >> 8)) & 0xf); }
	};

// --------------------------------------------------------------------------------

	template<COMMAND CMD, bool TLM_REQ = 0>
	class CommandFrame
	{
		CommandFrame(){}

	public: 
		static constexpr auto encode() { return (((static_cast<uint_fast8_t>(CMD) & VALUE_BITMASK) | VALUE_SHIFT) | 
											(((TLM_REQ?1:0) & TELEMETRY_BITMASK) << TELEMETRY_SHIFT) |
											_csm()); }

	private:
		static constexpr uint8_t _csm()
		{
			uint16_t csum_data = (static_cast<uint_fast8_t>(CMD) & VALUE_BITMASK) << 1 | 1;
			return (csum_data ^ (csum_data >> 4) ^ (csum_data >> 8)) & 0xf;
		}
	};
}/*DShot*/ } /*Protocol*/}/*opal*/