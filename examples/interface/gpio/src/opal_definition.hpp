
#pragma once

#include "hal/hal.hpp"

typedef opal::Hardware::Driver::NRF52840 SOC_t;

typedef opal::Hardware::Interface::GPIO::Port<0> PORT0_t;
typedef opal::Hardware::Interface::GPIO::Port<1> PORT1_t;

typedef opal::Hardware::Interface::GPIO::Pin<2> PIN2_t;
typedef opal::Hardware::Interface::GPIO::Pin<4> PIN4_t;

typedef opal::Hardware::Interface::GPIO::Instance<SOC_t, PORT1_t, PIN2_t> GPIO1_2;
typedef opal::Hardware::Interface::GPIO::Instance<SOC_t, PORT0_t, PIN4_t> GPIO0_4;