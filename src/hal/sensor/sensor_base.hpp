#pragma once

#include <cstdint>

#include "bus_base.hpp"

namespace opal { namespace Hardware 
{
	class Device
	{
	private:
		/* data */
	public:
		Device(/* args */);
		~Device();
	};
	
	static std::shared_ptr<opal::Hardware::Device> get_device_binding(int ident);
} } 

namespace opal { namespace Hardware { namespace Sensor 
{
	enum class TRIGGER : uint_fast8_t
	{
		MANUAL = 0,
		INTERRUPT,
		INTERRUPT_2,
		TIMER
	};	

// --------------------------------------------------------------------------------------------------

	template<typename T, uint_fast8_t SIZE>
	class Buffer : public std::array<T, SIZE>
	{
		public:
			Buffer(){}
			// virtual ~Buffer(){}

	};

// --------------------------------------------------------------------------------------------------

	enum class BUFFER_STATE : uint_fast8_t
	{ 
		READY = 0,
		PROCESSED,
	};

	template<typename T, uint_fast8_t SIZE>
	class NBuffered : public std::array<T, SIZE> 
	{
		public:
			NBuffered(){}
			// virtual ~Buffer(){}

		private: 
			std::array<BUFFER_STATE, SIZE> 	_state;
			uint_fast8_t 					_active_buf_ind;
	};

	template<typename T>
	using DoubleBuffered = NBuffered<T, 2>;

// --------------------------------------------------------------------------------------------------

template<typename T_VAL, typename T_TS>
	class Sample
	{
	public:
		Sample(){}

		T_VAL	value;
		T_TS 	ts;
	};

	// class SensorBase
	// {
	// public:
	// 	SensorBase(){}
	// 	virtual ~SensorBase(){}

	// 	void sample() { _sample_impl(); };

	// protected:
	// 	virtual void _sample_impl() = 0;

	// };

	template< class TRIGGER_SOURCE >
	class Trigger
	{
		public: 
			Triggger(){}
	};


	template<	typename TRAW,
				typename TOUT,
				class TRIGGER_SOURCE>
	class SensorBase : private TRIGGER_SOURCE
	{
		public: 
			SensorBase() {};

			void read()
			{
				_read_prepare();
				_read();
				_read_process();
			}

		protected:
			virtual void _read_prepare();
			virtual void _read();
			virtual void _read_process();
			virtual void _trigger_handler(const TRIGGER_TYPE trig);

		protected:
			TRAW _raw_buf;
			TOUT _out_buf;
	};

	template<	typename TRAW,
				typename TOUT,
				uint_fast8_t CHDIM >
	using FastSensor = SensorBase<	DoubleBuffered< Buffer<TRAW, CHDIM> >, 
									DoubleBuffered< Buffer<TOUT, CHDIM> > >;

} } }