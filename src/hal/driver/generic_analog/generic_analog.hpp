/**
 * @file generic_analog.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief Generic analog driver (ADC)
 * @version 0.1
 * @date 2020-11-15
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <stdint.h>
#include <functional>

/* HAL Includes
 * ----------------------
 */
#include <zephyr.h>
#include <logging/log.h>
#include <device.h>
#include <drivers/sensor.h>

/* App Includes
 * ----------------------
 */


/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */
// LOG_MODULE_REGISTER(IMU, LOG_LEVEL_DBG);


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */
namespace opal { namespace Hardware { namespace Driver
{
	template< class ADC >
	class GenericAnalog
	{
	public:
		GenericAnalog(){}
	};

} } }