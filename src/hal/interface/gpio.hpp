/**
 * @file gpio.hpp
 * @author Patrick Guenzel (patrickguenzel@outlook.com)
 * @brief GPIO interface implementation
 * @version 0.1
 * @date 2020-11-15
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#pragma once

/* #####################################################################
 * ##                         Includes                                ##
 * #####################################################################
 */

/* Stdlib Includes
 * ----------------------
 */
#include <cstdint>

/* HAL Includes
 * ----------------------
 */

/* App Includes
 * ----------------------
 */


/* #####################################################################
 * ##                     Typedef and Defines                         ##
 * #####################################################################
 */


/* #####################################################################
 * ##                       Static Variables                          ##
 * #####################################################################
 */


/* #####################################################################
 * ##                              API                                ##
 * #####################################################################
 */
namespace opal { namespace Hardware { namespace Interface { namespace GPIO
{

	template< uint_fast32_t ID >
	class Port
	{
		Port(){}
		public:
			static constexpr uint_fast32_t value = ID;
	};

	template< uint_fast32_t ID >
	class Pin
	{
		Pin(){}
		public:
			static constexpr uint_fast32_t value = ID;
	};

	enum class DIR : uint_fast8_t
	{
		INPUT = 0,
		OUTPUT
	};

	enum class DRIVE_STRENGTH : uint_fast8_t
	{
		NORMAL = 0,
		STRONG = 3,
		STRONG_LOW_NORMAL_HIGH = 1,
		NORMAL_LOW_STRONG_HIGH = 2,
		DISCONNECT_LOW_NORMAL_HIGH = 4,
		DISCONNECT_LOW_STRONG_HIGH = 5,
		NORMAL_LOW_DISONNECT_HIGH = 6,
		STRONG_LOW_DISONNECT_HIGH = 7
	};

	enum class PULL : uint_fast8_t
	{
		NONE = 0,
		UP = 1,
		DOWN = 2
	};

	enum class IRQ : uint_fast8_t
	{
		NONE = 0,
		HIGH_LEVEL,
		LOW_LEVEL,
		RISING_EDGE,
		FALLING_EDGE,
		BOTH_EDGES
	};

	template< class DRIVER, class PORT, class PIN >
	class Instance
	{
			Instance(){}
		public: 

			static void 		set(const bool val) { DRIVER::template GPIOPeripheral<PORT, PIN>::set(val); }
			static const bool 	get(void) { return DRIVER::template GPIOPeripheral<PORT, PIN>::get(); }
			static void 		configure(	const DIR io_dir, 
											const PULL io_pull, 
											const IRQ io_irq, 
											const DRIVE_STRENGTH io_pwr)
			{ DRIVER::template GPIOPeripheral<PORT, PIN>::configure(io_dir, io_pull, io_irq, io_pwr); }

			static constexpr auto port_num(void) { return PORT::value; }
			static constexpr auto pin_num(void) { return PIN::value; }
	};
	
} } } }